import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Scanner;

public class Exo31 {

	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");

		CallableStatement pcActeurExistant = c.prepareCall("{ ? = call acteur_existant( ?, ?)}");
		pcActeurExistant.registerOutParameter(1, Types.INTEGER);
		Scanner sc = new Scanner(System.in);

		String nomActeur = null;
		String prenomActeur = null;

		while (nomActeur == null || nomActeur.equals("")) {
			System.out.print("Entrez le nom de l'acteur : ");
			nomActeur = sc.nextLine();
			nomActeur = nomActeur.toUpperCase();
		}

		while (prenomActeur == null || prenomActeur.equals("")) {
			System.out.print("Entrez le pr�nom de l'acteur : ");
			prenomActeur = sc.nextLine();
		}

		pcActeurExistant.setString(2, nomActeur);
		pcActeurExistant.setString(3, prenomActeur);

		pcActeurExistant.execute();

		if (pcActeurExistant.getInt(1) == 0) {
			System.out.println("L'acteur n'existe pas dans la base de donn�e");
		} else {
			System.out.println("L'acteur existe dans la base de donn�e avec l'id " + pcActeurExistant.getInt(1));
		}
	}

}
