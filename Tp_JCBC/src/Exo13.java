import java.sql.*;

/**
 * 
 */

/**
 * @author bernar323u
 *
 */
public class Exo13 {

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");
		PreparedStatement ps = c.prepareStatement("Select * From Acteur");
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData rsmd = rs.getMetaData();
		System.out.println("Meta-donn�es de la table Acteur :\n   Nombre de colonnes = " + rsmd.getColumnCount());
		for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
			System.out.println(
					"   La colone " + rsmd.getColumnName(i) + " n�" + i + " est de type " + rsmd.getColumnTypeName(i));
		}
		
		rs.close();
		ps.close();
		c.close();
	}

}
