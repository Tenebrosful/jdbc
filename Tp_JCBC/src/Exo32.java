import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author bernar323u
 *
 */
public class Exo32 {

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");
		
		CallableStatement pcAjouterRole = c.prepareCall("{ call AjouterRole( ?, ?, ?)}");

		Scanner sc = new Scanner(System.in);

		int idActeur = 0, idFilm = 0;
		
		pcAjouterRole.registerOutParameter(3, Types.INTEGER);

		System.out.print("Entrez l'id de l'acteur : ");
		idActeur = sc.nextInt();
		pcAjouterRole.setInt(1, idActeur);

		System.out.print("Entrez l'id du film : ");
		idFilm = sc.nextInt();
		pcAjouterRole.setInt(2, idFilm);
		
		pcAjouterRole.execute();
		
		System.out.println(pcAjouterRole.getInt(3));
		
		pcAjouterRole.close();
		c.close();
	}

}
