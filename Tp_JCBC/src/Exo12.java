import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 
 */

/**
 * @author BERNARD Hugo 'Tenebrosful'
 *
 */
public class Exo12 {

	/**
	 *
	 * @throws SQLException
	 */
	public static void main(String[] args0) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");
		PreparedStatement ps = c.prepareStatement("UPDATE Film set entree = 500000 where idfilm = 14");
		int succes = ps.executeUpdate();
		if (succes == 1)
			System.out.println("Le film a bien �t� actualis�");
		else
			System.out.println("Une erreur est survenue");

		ps.close();
		c.close();
	}

}
