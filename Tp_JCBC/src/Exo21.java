import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 */

/**
 * @author bernar323u
 *
 */
public class Exo21 {

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");
		PreparedStatement psNomActeur = c
				.prepareStatement("Select idActeur, nom, prenom From Acteur Order By nom ASC, prenom ASC");
		PreparedStatement psListeFilm = c.prepareStatement(
				"Select count(IDFILM) nbrFilm, to_number(to_char(dateSortie,'YYYY')) anneeSortie From Jouer Natural Join Film Where idActeur = ? and (to_char(dateSortie,'YYYY') = '2010' or to_char(dateSortie,'YYYY') = '2011') GROUP BY IdActeur, to_char(DATESORTIE,'YYYY') Order By anneeSortie ASC",
				ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

		ResultSet rsNomActeur = psNomActeur.executeQuery();

		while (rsNomActeur.next()) {
			System.out.println("\n" + rsNomActeur.getString("nom") + " " + rsNomActeur.getString("prenom"));

			psListeFilm.setInt(1, rsNomActeur.getInt("idacteur"));

			ResultSet rsListeFilm = psListeFilm.executeQuery();
			rsListeFilm.last();

			if (rsListeFilm.getRow() == 0) {
				System.out.println("   Aucun film");
			} else {
				rsListeFilm.beforeFirst();
				int i = 0;
				while (rsListeFilm.next()) {
					int annee = rsListeFilm.getInt("anneeSortie");
					if (i == 0 && annee == 2011) {
						System.out.println("\n Pour l'ann�e 2010 : Aucun film");
						System.out.println("\n Pour l'ann�e " + annee + " : " + rsListeFilm.getInt("nbrFilm"));
					} else {
						System.out.println("\n Pour l'ann�e " + annee + " : " + rsListeFilm.getInt("nbrFilm"));
						if (i == 0 && rsListeFilm.last())
							System.out.println("\n Pour l'ann�e 2011 : Aucun film");
					}
					i += 1;
				}
			}
		}

		rsNomActeur.close();
		psListeFilm.close();
		c.close();
	}
}
