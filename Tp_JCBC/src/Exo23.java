import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 */

/**
 * @author bernar323u
 *
 */
public class Exo23 {

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");
		PreparedStatement psListeFilm = c.prepareStatement(
				"Select idFilm, titre, to_char(DATESORTIE,'DD/MM/YYYY') dateSortie, paysFilm From Film Order By paysFilm, titre",
				ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		PreparedStatement psListeActeur = c.prepareStatement(
				"Select nom, prenom From Acteur Natural Join Jouer Where idFilm = ?", ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_READ_ONLY);

		ResultSet rsListeFilm = psListeFilm.executeQuery();

		String lastPaysFilm = "";

		rsListeFilm.last();
		if (rsListeFilm.getRow() == 0) {
			System.out.println("Aucun film existant");
		} else {
			rsListeFilm.beforeFirst();
			while (rsListeFilm.next()) {
				if (!rsListeFilm.getString("paysFilm").equals(lastPaysFilm)) {
					System.out.println("\n\n" + rsListeFilm.getString("paysFilm"));
					lastPaysFilm = rsListeFilm.getString("paysFilm");
				}

				System.out.println(
						"\n   " + rsListeFilm.getString("titre") + " sorti le " + rsListeFilm.getString("dateSortie"));

				psListeActeur.setInt(1, rsListeFilm.getInt("idFilm"));
				ResultSet rsListeActeur = psListeActeur.executeQuery();

				rsListeActeur.last();
				if (rsListeActeur.getRow() == 0) {
					System.out.println("Aucun acteur.trice");
				} else {
					rsListeActeur.beforeFirst();
					while (rsListeActeur.next()) {
						System.out.println(
								"     " + rsListeActeur.getString("nom") + " " + rsListeActeur.getString("prenom"));
					}
				}
			}

		}

		rsListeFilm.close();
		psListeActeur.close();
		psListeFilm.close();
		c.close();
	}

}
