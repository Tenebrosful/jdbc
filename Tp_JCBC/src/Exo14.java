import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author bernar323u
 *
 */
public class Exo14 {

	static Scanner sc = new Scanner(System.in);

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");
		PreparedStatement ps = c.prepareStatement("Select titre From Film Where paysFilm = ?");
		System.out.print("Entrez un pays : ");
		String pays = sc.nextLine();
		ps.setString(1, pays);

		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			System.out.println(rs.getString("titre"));
		}

		ps.close();
		c.close();

	}

}
