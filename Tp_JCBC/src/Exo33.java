import java.sql.*;

/**
 * 
 */

/**
 * @author bernar323u
 *
 */
public class Exo33 {

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@charlemagne.iutnc.univ-lorraine.fr:1521:infodb",
				"bernar323u", "mewmew");

		Statement st = c.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

		ResultSet rs = st.executeQuery("Select entree From Film Where idFilm = 13");

		while (rs.next()) {
			rs.updateInt("entree", rs.getInt("entree") + 10000);
		}

		rs.updateRow();

		rs.close();
		st.close();
		c.close();
	}

}
